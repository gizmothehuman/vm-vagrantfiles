### Vagrant library of machine images

Welcome to the QA library of machine images, which is managed using [Vagrant](https://www.vagrantup.com/intro/getting-started/index.html) and [Hashicorp](https://www.hashicorp.com/)'s [Vagrant box repository](https://app.vagrantup.com/boxes/search). You can find [the Vagrant docs here](https://www.vagrantup.com/docs/index.html), as well as read a summary of how to use these images below.

## Prerequisites

Before you'll be able to use the VMs described in this repository, you'll need to have the following installed:

* Virtualbox - this is a free Oracle tool
* Vagrant CLI - install this using [the installers on this page](https://www.vagrantup.com/downloads.html)


## Quickstarting a VM for manual or automated testing

Each folder contains one file*** called a Vagrantfile that uses a [Ruby DSL](https://robots.thoughtbot.com/writing-a-domain-specific-language-in-ruby) to describe the preferred configuration for a virtual machine image. To start a VM, change directories in your shell to the image you would like to use, e.g.

```
$ git clone git@bitbucket.org:<your BitBucket username here>/vm-vagrantfiles.git
$ cd vm-vagrantfiles
$ cd Windows10_Edge  #this will start a windows VM with Edge installed
$ vagrant up
```

## Editing and adding new images

The default images are called "boxes" in Vagrant terminology. The default box is run in a headless (no GUI) state ([read here for more info](https://www.vagrantup.com/docs/virtualbox/configuration.html#gui-vs-headless)), however we have added this configuration block to each Vagrantfile ensure that a GUI is spawned for manual testing:

```
config.vm.provider "virtualbox" do |v|
  v.gui = true
end
```



*** I know this is weird, sorry about that. Vagrantfiles are designed to go in a folder with whatever other app code you want to run _on_ the VM, but since we are using it for desktop app testing we only need the Vagrantfile in one folder. Vagrant needs that to be the only Vagrantfile in a folder or it will get confused. 